# IFS_project_stub_python #

## Clone The Project
_______

1. Clone the project to your IDE
2. change to the directory in your CLI
3. Create a virtual environment with `python -m venv venv`
4. pip install requirements.txt `pip install requirements.txt`

## test the API
1. run `python -m pytest -v

## Description
_______

> Integrates jotform with keap max classic by listening to a webhook from jotform and POSTing the data to your Keap database.  
> It will check for duplicates on the email, link the contact to their company, and update the custom fields on the contact and company records.

