import base64
from classes.MyInfusionsoft import MyInfusionsoft
import requests
import logging
from urllib.parse import urlencode
from flask import Flask, redirect, url_for, request, render_template

from config import settings
from classes.TokenStorage import TokenStorage
from classes.Keap import Keap

app = Flask(__name__)

AUTHORIZE_URL = "https://signin.infusionsoft.com/app/oauth/authorize"
ACCESS_TOKEN_URL = "https://api.infusionsoft.com/token"
BASE_URL = "https://api.infusionsoft.com/crm/rest/v1/"


keap = Keap()
ifs = MyInfusionsoft

@app.route("/")
def load_index():
    token = TokenStorage.get_infusionsoft_oauth_token()    
    if not token or not token.get("access_token"):
        app.logger.info("token not found, redirecting to authorization page")
        return render_template("authorize.html")
    else:
        ifs.get_MyInfusionsoft()
        refresh_access_token()
        keap.set_session(ifs.get_MyInfusionsoft())
        app.logger.info("token was found")
        if ifs.connected:
            ifs_connection_status = "CONNECTED"
        else:
            ifs_connection_status = "NOT CONNECTED"                        
        return render_template(
            "status.html",
            connectionstatus=ifs_connection_status,
        )


@app.route("/authorize")
def authorize():
    code = request.args.get("code", "")
    scope = request.args.get("scope", "")
    data = {
        "client_id": settings.CLIENT_ID,
        "redirect_uri": settings.REDIRECT_URL,
        "response_type": "code",
        "scope": scope,
    }
    if not code:
        app.logger.info(
            "auth_code not found, redirecting to oauth_url for user authentication"
        )
        oauth_url = AUTHORIZE_URL + "?" + urlencode(data)
        return redirect(oauth_url)
    else:
        data["client_secret"] = settings.CLIENT_SECRET
        data["code"] = code
        data["grant_type"] = "authorization_code"
        authorization_token_response = requests.post(ACCESS_TOKEN_URL, data)
        token = authorization_token_response.json()
        app.logger.info("token successfully retrieved, writing to file")
        TokenStorage.write_infusionsoft_oauth_token(token)
        app.logger.info("successfully wrote token to file")
        return redirect(url_for("load_index"))


@app.route("/refresh-access-token/")
def refresh_access_token():
    app.logger.info("attempting to refresh access token")
    token = TokenStorage.get_infusionsoft_oauth_token()
    data = {
        "grant_type": "refresh_token",
        "refresh_token": token.get("refresh_token"),
    }
    auth_header = (
        "Basic "
        + base64.b64encode(
            bytes(f"{settings.CLIENT_ID}:{settings.CLIENT_SECRET}", "utf-8")
        ).decode()
    )
    refresh_token_response = requests.post(
        ACCESS_TOKEN_URL, data, headers={"Authorization": auth_header}
    )
    token = refresh_token_response.json()
    TokenStorage.write_infusionsoft_oauth_token(token)
    app.logger.info("Your access token was refreshed successfully")
    return redirect(url_for("load_index"))


# webhook
@app.route("/webhook", methods=["POST"])
def webhook():
    if not keap.session_established:
        keap.set_session(ifs.get_MyInfusionsoft())      
    json_data = request.json
    app.logger.info("POST received from webhook, building Keap contact")
    content = json_data

    company_id = keap.get_company_id(content)
    if company_id:
        contact = keap.build_contact(content, company_id)
        keap.add_keap_contact(contact)
    return content


if __name__ == "__main__":
    settings.LOG_HANDLER.setFormatter(settings.LOG_FORMATTER)
    app.logger.addHandler(settings.LOG_HANDLER)
    app.logger.setLevel(logging.DEBUG)
    app.logger.info("Starting app")
    app.run(debug=settings.DEBUG)

