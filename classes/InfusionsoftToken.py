from time import time


class InfusionsoftToken:
    def __init__(self, **kwargs):
        self.access_token = kwargs.get("access_token", None)
        self.refresh_token = kwargs.get("refresh_token", None)
        self.expires_in = kwargs.get("expires_in", 0)
        self.end_of_life = int(time()) + self.expires_in
        self.scope = kwargs.get("scope", None)

    def is_expired(self):
        return self.end_of_life < int(time())

    def get_token(self):
        token = {
            "expires_in": self.expires_in,
            "token_type": "bearer",
            "refresh_token": self.refresh_token,
            "access_token": self.access_token,
            "scope": self.scope,
        }
        return token
