from config.log_config import get_logger
from config.settings import IFS_FIELD_MAP


class Keap:

    def __init__(self):
        self.session = None        
        self.logger = self.get_default_logger()        

    @property
    def session_established(self):
        return self.session or False

    def set_session(self, session):
        self.session = session
        self.session.oauth_connect()

    def set_logger(self, logger):
        self.logger = logger

    def get_default_logger(self):
        return get_logger(__name__)

    def build_contact(self, content, company_id):
        self.logger.info("Building Contact")
        contact_attributes = {
            "FirstName": content["q3_name"]["first"],
            "LastName": content["q3_name"]["last"],
            "Email": content["q4_email"],
            "StreetAddress1": content["q5_address"]["addr_line1"],
            "StreetAddress2": content["q5_address"]["addr_line2"],
            "City": content["q5_address"]["city"],
            "State": content["q5_address"]["state"],
            "PostalCode": content["q5_address"]["postal"],
            "Phone1": content["q6_phoneNumber"]["full"],
            "Company": content["q11_companyName"],
            "CompanyID": company_id,
        }
        if "FAVORITE_COLOR" in IFS_FIELD_MAP:
            contact_attributes[IFS_FIELD_MAP["FAVORITE_COLOR"]] = content[
                "q9_favoriteColor"
            ]
        if "REASON_FOR_REQUEST" in IFS_FIELD_MAP:
            contact_attributes[IFS_FIELD_MAP["REASON_FOR_REQUEST"]] = content[
                "q8_reasonFor"
            ]

        return contact_attributes

    def add_keap_contact(self, contact):
        try:
            contact_id = self.session.ContactService("addWithDupCheck", contact, "Email")
            self.logger.info(f"successfully added Contact to Keap. Contact ID:{contact_id}")
            return contact_id
        except Exception as e:
            self.logger.exception(e)
            return False

    def retrieve_company(self, company_name, company_industry):
        table = "Company"
        returnFields = ["Company", "CompanyID"]
        query = {"Company": company_name, "_Industry": company_industry}
        limit = 1
        page = 0
        company = self.session.DataService("query", table, limit, page, query, returnFields)
        if company and len(company) and company[0]:
            return company[0]
        return None

    def create_company(self, companyData):
        table = "Company"
        values = companyData
        return self.session.DataService("add", table, values)

    def get_company_id(self, content):
        company_attributes = {
            "Company": content["q11_companyName"],
        }
        if "COMPANY_INDUSTRY" in IFS_FIELD_MAP:
            company_attributes[IFS_FIELD_MAP["COMPANY_INDUSTRY"]] = content["q13_industry"]
        if "COMPANY_CERTIFICATE_NUMBER" in IFS_FIELD_MAP:
            company_attributes[IFS_FIELD_MAP["COMPANY_CERTIFICATE_NUMBER"]] = content[
                "q15_certificateNumber"
            ]
        if "COMPANY_COMMUNITY" in IFS_FIELD_MAP:
            company_attributes[IFS_FIELD_MAP["COMPANY_COMMUNITY"]] = content[
                "q16_community"
            ]
        if "BUSINESS_TYPE" in IFS_FIELD_MAP:
            company_attributes[IFS_FIELD_MAP["BUSINESS_TYPE"]] = content["q17_businessType"]

        try:
            contact_company = self.retrieve_company(
                company_attributes["Company"], company_attributes["_Industry"]
            )
            if contact_company == None:
                self.logger.info("Company did not exist, creating company")
                company_id = self.create_company(company_attributes)
                self.logger.info(f"Company created ID: {company_id}")
                return company_id
            else:
                company_id = contact_company["CompanyID"]
                self.logger.info(f"Company exists ID:{company_id}")
                return company_id
        except Exception as e:
            self.logger.exception(e)
            return False
