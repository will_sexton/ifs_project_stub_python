from config.log_config import get_logger
import logging

from classes.InfusionsoftToken import InfusionsoftToken
from classes.TokenStorage import TokenStorage
from config.settings import CLIENT_ID, CLIENT_SECRET, APP_NAME, REDIRECT_URL
from config.settings import LOG_FORMATTER, LOG_HANDLER

try:
    from xmlrpc.client import ServerProxy, Error
except:
    from xmlrpclib import ServerProxy, Error
try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode


class MyInfusionsoft:
    # TODO: You don't ever really want to pass config variables right into your class. It makes it tightly coupled
    #       to that design pragma. Best practice is to add getter/setter methods for things like logging.
    logger = get_logger(__name__)


    def __init__(self, **kwargs):        
        self.logger.info("Creating MyInfusionsoft object")
        self.client = None
        self.token = kwargs.get("token", InfusionsoftToken())
        self.rest_uri = "https://api.infusionsoft.com/crm/xmlrpc/v1?access_token={}"

    @property
    def connected(self):
        return self.client or False

    # def get_default_logger(self):
    #     self.logger = get_logger(__name__)
    #     print(self.logger)
    #     return get_logger(__name__)

    def __getattr__(self, service):
        def function(method, *args):
            call = getattr(self.client, service + "." + method)
            try:
                return call(self.key, *args)
            except self.client.error as v:
                return "ERROR", v

        return function

    def oauth_connect(self):
        uri = self.rest_uri.format(self.token.access_token)
        self.logger.info(
            f"Connecting to Infusionsoft Oauth at {self.rest_uri}"
        )
        if not self.token.access_token:
            self.logger.exception("There is no valid access token")
            raise ValueError("There is no valid access token")
        self.client = ServerProxy(uri, use_datetime=self.use_datetime)
        self.client.error = Error
        self.key = self.token.access_token
        return self.client

    def is_token_expired(self):
        return self.token.is_expired()

    def get_token(self):
        return self.token.get_token()


    # TODO: I wouldn't call your class MyInfusionsoft, just use Infusionsoft or more correct now, Keap.
    #       Which means your Keap class, should be named something like Helper, JotformToKeap, etc.
    @classmethod
    def get_MyInfusionsoft(self):
        token = TokenStorage.get_infusionsoft_oauth_token()
        logger = get_logger(__name__)
        config = {
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "redirect_uri": REDIRECT_URL,
            "debug": True,
            "app_name": APP_NAME,
            "token": InfusionsoftToken(**token),
        }
        ifs = self(**config)
        self.logger.info("returning infusionsoft object")
        return ifs
