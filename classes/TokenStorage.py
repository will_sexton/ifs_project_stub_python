from config.log_config import get_logger
import json

from config.settings import TOKEN_PATH
logger = get_logger(__name__)

class TokenStorage:
    @staticmethod
    def get_infusionsoft_oauth_token():       
        try:            
            logger.info("Getting: " + TOKEN_PATH)
            with open(TOKEN_PATH, "r", encoding="utf-8") as f:
                data = json.load(f)
                return data
        except FileNotFoundError:
            logger.warning("Auth Token was not found at: " + TOKEN_PATH)
            token = {}
            with open(TOKEN_PATH, "w", encoding="utf-8") as f:
                json.dump(token, f, ensure_ascii=False, indent=4)
            return None


    @staticmethod
    def write_infusionsoft_oauth_token(token):
        logger.info("writing auth token to file")
        with open(TOKEN_PATH, "w", encoding="utf-8") as f:
            json.dump(token, f, ensure_ascii=False, indent=4)
