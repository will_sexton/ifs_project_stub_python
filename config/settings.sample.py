import logging
import pathlib
import os
from logging.handlers import RotatingFileHandler

# Your credentials can be found at https://keys.developer.keap.com/
CLIENT_ID = ""
CLIENT_SECRET = ""
APP_NAME = ""


MY_SITE = "mysite.site.com/"
REDIRECT_URL = MY_SITE + "authorize"

# TOGGLE DEBUG MODE (True / False)
DEBUG = True

# INFUSIONSOFT Field Mappings
"""
Enter the names of your custom fields inside of the parenthesis next to the right of the local name
example: 
IFS_FIELD_MAP = {
    'FAVORITE_COLOR': '_FavoriteColor', 
    'REASON_FOR_REQUEST': '_ReasonforRequest',
    }
"""
IFS_FIELD_MAP = {
    "FAVORITE_COLOR": "",  # Keap custom field name for favorite color
    "REASON_FOR_REQUEST": "",  # Keap custom field name for reason for request
    "COMPANY_INDUSTRY": "",  # Keap custom field name for a company's Industry
    "COMPANY_CERTIFICATE_NUMBER": "",  # Keap custom field name for certificate number
    "COMPANY_COMMUNITY": "",  # Keap custom field name for company's community
    "BUSINESS_TYPE": "",  # Keap custom field name for a company's business type
}

# App Constants
BASE_DIR = str(pathlib.Path(__file__).parent.parent.resolve())
CONFIG_PATH = os.path.join(BASE_DIR, "config", "config.json")
TOKEN_PATH = os.path.join(BASE_DIR, "config", "token.json")
LOG_DIR = os.path.join(BASE_DIR, "logs")
LOG_FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
LOG_HANDLER = RotatingFileHandler(LOG_DIR + "/log.log", maxBytes=10000, backupCount=1)
