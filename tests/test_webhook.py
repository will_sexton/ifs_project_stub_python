import json
import pytest
from app import app

dummy_data = "tests/webhook_response_sample.json"


@pytest.fixture
def client():
    """Configures the app for testing

    Sets app config variable ``TESTING`` to ``True``

    :return: App for testing
    """
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client


def test_app(client):
    webhook_response = get_webhook_sample_data()
    rv = client.post("/webhook", json=webhook_response)
    assert rv.status_code == 200


def get_webhook_sample_data():
    with open(dummy_data, "r", encoding="utf-8") as f:
        data = json.load(f)
        return data
